import defaultAxios from "axios"

const axios = defaultAxios.create({
    baseURL: 'http://jsonplaceholder.typicode.com/',
    headers: {'Content-Type': 'application/json'}
});

export const getAllTodos = async () => {
    try {
        const todos = await axios.get('todos?_limit=5')

        return todos.data
    } catch(err) {
        return console.error(err)
    }
}