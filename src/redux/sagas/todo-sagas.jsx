import {
    put,
    call,
    takeLatest,
    takeEvery
  } from 'redux-saga/effects'
import { CLEAR_TODO_TITTLE } from '../action/todo-action'
import {
    SET_LOADING,
    GET_TODOS,
    GET_TODOS_REQUESTED,
    SET_TODO_TITLE,
    SET_TODO_TITLE_REQUESTED,
    CLEAR_TODO_TITLE,
    CREATE_TODO,
    CREATE_TODO_REQUESTED,
    DELETE_TODO,
    DELETE_TODO_REQUESTED
} from '../actions/todo-action'
import {
    getAllTodos,
    createNewTodo,
    deleteExistedTodo
} from '../api/todo-api'

//Create Todo
function* createTodo ({ payload }) {
    yield put({ type: SET_LOADING })

    const newTodo = yield call(createNewTodo, payload)
    yield put({ type: CREATE_TODO, payload: newTodo})

    yield put({ type: CLEAR_TODO_TITTLE})

}

//Delete Todo
function* deleteTodo({ payload }) {
    yield put({ type: SET_LOADING })
    const todo = yield call(deleteExistedTodo, payload)
    
}
