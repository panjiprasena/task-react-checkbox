import React from "react"
import Header from "./Header"
import Footer from "./footer"
import List from "./toDoList"

function App() {
    return (
        <div>
            <Header />
                <List />
            <Footer />
        </div>
    )
}

export default App;