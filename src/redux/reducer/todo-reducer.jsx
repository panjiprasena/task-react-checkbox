import {
    SET_LOADING,
    GET_TODOS,
    SET_TODO_TITTLE,
    CREATE_TODO,
    DELETE_TODO,
    CLEAR_TODO_TITTLE
} from "../action/todo-action"

//Define State
const initialState = {
    loading: false,
    todos: [],
    tile: ""
}

export default(state = initialState, {type, payload}) => {
    switch(type) {
        case SET_LOADING:
            return {
                ...state,
                loading: true
            }
        case GET_TODOS:
            return {
                ...state,
                title: payload
            }
        case SET_TODO_TITTLE:
            return {
                    ...state,
                    title: payload  
            }
        case CREATE_TODO:
            return {
                ...state,
                todos: [payload, ...state.todos]
            }
        case CLEAR_TODO_TITTLE:
            return {
                ...state,
                title: ""
            }
        case DELETE_TODO:
            return {
                ...state,
            todos: state.todos.filter(todo => todo.id !== payload),
            loading: false
            }
        default:
            return state
    }
}