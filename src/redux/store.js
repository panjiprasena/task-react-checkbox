import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension"
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas"
import rootReducer from ".reducer"

const sagaMidlleware = createSagaMiddleware()
const store = createStore(
    rootReducer,
    {},
    composeWithDevTools(applyMiddleware(sagaMidlleware))
)

sagaMidlleware.run(rootSaga)

export default store;