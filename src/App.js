import React from "react"
import Header from "./components/Header"
import Footer from "./components/footer"
import List from "./components/toDoList"
import ToDo from "./components/toDoItem"
import TodoData from "./components/TodoItemData"
import todoItemData from "./components/TodoItemData"

class App extends React.Component {
    constructor() {
        super()
        this.state = {
            tugas: todoItemData
        }
        this.handleChange = this.handleChange.bind(this)
    }
    
    handleChange(id) {
        this.setState(prevState => {
            const updatedTugas = prevState.tugas.map(toDo => {
                if (toDo.id === id) {
                    toDo.selesai = !toDo.selesai
                }
                return toDo
            })
            return {
                tugas: updatedTugas
            }
        })
    }
    render() {
    let data = TodoData
    return (
        <div>
            <Header />
            {data.map(dat => {
                return (<ToDo nama = {dat.nama}/>)
                
            })}
            <Footer />
        </div>
    )
    }
}

export default App;