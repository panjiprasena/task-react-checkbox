const todoItemData = [
    {
        id: 1,
        nama: "Membuang Sampah",
        selesai: false
    },
    {
        id: 2,
        nama: "Belanja",
        selesai: false
    },
    {
        id: 3,
        nama: "Membersihkan Rumah",
        selesai: true
    },
    {
        id: 4,
        nama: "Memberi makan ikan",
        selesai: true
    },
    {
        id: 5,
        nama: "Olahraga",
        selesai: false
    },
    {
        id: 6,
        nama: "Makan",
        selesai: true
    }
]

export default todoItemData;